from todo_app.view_model import ViewModel
from todo_app.data.trello_items import Item
def test_viewmodel_done():
    #arrange
    items=[
        Item(id=1,name='dummy_test',status='Done'),
        Item(id=2,name='Clean the house',status='To Do'),
        Item(id=3,name='Walk the dog',status='To Do'),
        Item(id=4,name='finish this exercise',status='To Do'),
        Item(id=5,name='Coffee',status='To Do')
    ]
    item_view_model = ViewModel(items)
    
    #Action 'done' item
    result_done_items=item_view_model.done_items
    #assert
    assert len(result_done_items)==1
    item=result_done_items[0]
    assert item.status=='Done'
    
    #Action 'to do' item 
    result_todo_items=item_view_model.todo_items
    #assert
    assert len(result_todo_items)==4
    item=result_todo_items[3]
    assert item.status=='To Do'