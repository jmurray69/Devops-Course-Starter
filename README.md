# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

```
Following command used to build and run prod and dev environments. Note Flask dev uses moount to hot reload changes in source

#Production

docker build --tag todo-app:prod . --target production
docker run --env-file .env -it -p 8000:8000 todo-app:prod

#Development

docker build --tag todo-app:dev . --target development
docker run --env-file .env -it -p 8000:5000 --mount type=bind,source="$(pwd)"/todo_app,target=/app/todo_app todo-app:dev
```

# CICD Pipeline created with basic test (exercise 3 is not yet completed)
Following added to .gitlab-ci.yaml:
```
- docker build --target test --tag my-test-image .
- docker run --env-file .env.test my-test-image test
```
Exercise - 3 :
Run the tests using the following command:
```
poetry run pytest
```


# Exercise-8 - Docker prod container successfully deployed to Azure

```
DockerHub 'To-do' prod: https://hub.docker.com/r/jmu1969/todo-app
Run the following commands to push to Docker: 
- docker build --tag jmu1969/todo-app:prod . --target production
- docker push jmu1969/todo-app:prod   
- Add WEBSITE_PORT 8000 to config settings in Webapp service so Azure tests the correct port
```
now visit Todo website hosted in Azure: ['https://todo-or-not-todo.azurewebsites.net'](https://todo-or-not-todo.azurewebsites.net)

# Exercise-9 - Publishing by CD pipeline
Added 2nd job to gitlab-ci.yaml  as follows:
``` - docker build . --tag jmu1969/todo-app:latest
    - echo $DOCKER_TOKEN | docker login docker.io --username jmu1969 --password-stdin
    - docker push jmu1969/todo-app:latest
    - apk add --update-cache curl
    - curl -dH --fail -X POST $AZURE_WEBHOOK
``` 
Also added the following rules to only run when exercise-9 branch commit occurs. This can be changed to main when merge is completed.
```
  rules:
    - if: $CI_COMMIT_BRANCH == "exercise-9"
```
2 variables added to pipeline to obscure from logs:
```
- AZURE_WEBHOOK (Azure webhook to redeploy)
- DOCKER_TOKEN  (docker password)
```