import requests, os

def get_cards():
    #response=requests.get (url=f"https://api.trello.com/1/boards/62cec6917f767a0862e96bbb/lists?key={os.getenv('TRELLO_APIKEY')}&token={os.getenv('TRELLO_TOKEN')}&cards=open")
    response=requests.get (url=f"https://api.trello.com/1/boards/{os.getenv('TRELLO_BOARD')}/lists?key={os.getenv('TRELLO_APIKEY')}&token={os.getenv('TRELLO_TOKEN')}&cards=open")
  
    #print(response.json())
    #print(response.json()[0]['cards'][0]['name'])
    items=[]
    for list in response.json():
        list_name=list['name']
        for card in list['cards']:
            id=card['id']
            name=card['name']
            #item = { 'id': id, 'title': name, 'status': list_name }
            item = Item(id,name,list_name)
            items.append(item)
    return items

def new_card(title):
    url = f"https://api.trello.com/1/cards"
    querystring = {"name": title, "idList": {os.getenv('TRELLO_TODOLISTID')}, "key": {os.getenv('TRELLO_APIKEY')}, "token": {os.getenv('TRELLO_TOKEN')}}
    response = requests.request("POST", url, params=querystring)
    card_id = response.json()["id"]
    return card_id

def move_card(cardid):
    url =f"https://api.trello.com/1/cards/{cardid}"
    querystring = {"idList": {os.getenv('TRELLO_DONELISTID')}, "key": {os.getenv('TRELLO_APIKEY')}, "token": {os.getenv('TRELLO_TOKEN')}}
    # PUT /1/cards/{cardID}?idList={listID}
    response = requests.request("PUT", url, params=querystring)
   
class Item:
    def __init__(self, id, name, status = 'To Do'):
        self.id = id
        self.title = name
        self.status = status

