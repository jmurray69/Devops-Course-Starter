from calendar import c
#from turtle import title
from flask import Flask
from flask import render_template, request, redirect

from todo_app.flask_config import Config
#from todo_app.data.session_items import get_items, add_item
from todo_app.data import trello_items
from todo_app.view_model import ViewModel


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())

    @app.route('/')
    def index():
        items = trello_items.get_cards()
        trello_items.get_cards()
        #return render_template("index.html", items = items)
        item_view_model = ViewModel(items)
        return render_template('index.html', view_model=item_view_model)    
    
    

    #Create New Card on Todo list
    @app.route('/add', methods=['POST'])
    def add():
        title = request.form["todo"]
        todo = trello_items.new_card(title)
        return redirect('/')

    #Move Card to Done
    @app.route('/complete_item/<cardid>', methods=['GET'])
    def complete_item(cardid):
        trello_items.move_card(cardid)
        return redirect('/')

    return app