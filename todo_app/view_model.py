class ViewModel:
    def __init__(self, items):
        self._items = items

    @property
    def items(self):
        return self._items
    @property
    def done_items(self):
        done=[]
        for item in self.items:
                if (item.status == "Done"):
                    done.append(item)
        return done
    @property
    def todo_items(self):
        todo=[]
        for item in self.items:
                if (item.status == "To Do"):
                    todo.append(item)
        return todo
    