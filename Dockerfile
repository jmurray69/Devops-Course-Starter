FROM python:3.8-slim-buster as base
ENV POETRY_VERSION=1.2.0
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VENV=/opt/poetry-venv
ENV POETRY_CACHE_DIR=/opt/.cache

# Install poetry separated from system interpreter
RUN python3 -m venv $POETRY_VENV \
    && $POETRY_VENV/bin/pip install -U pip setuptools \
    && $POETRY_VENV/bin/pip install poetry==${POETRY_VERSION}
 
# Add `poetry` to PATH
ENV PATH="${PATH}:${POETRY_VENV}/bin"

WORKDIR /app

# Install dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry install

COPY todo_app ./todo_app



FROM base as production
# Configure for production
ENTRYPOINT ["poetry", "run", "gunicorn", "--bind", "0.0.0.0", "todo_app.app:create_app()"]

FROM base as development
# Configure for local development
ENTRYPOINT ["poetry", "run", "flask", "run", "--host", "0.0.0.0"]

# testing stage
FROM base as test
COPY test ./test

ENTRYPOINT ["poetry", "run", "pytest"]